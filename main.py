import pyodbc
from time import strftime, gmtime
import os
import csv
from collections import OrderedDict
import Site
import Order

max_requests = 10000
products = [0] * 10
X = 28


def connect_to_server(db_name):
    connection = pyodbc.connect(Driver='{SQL Server};', Server='technionddscourse.database.windows.net',
                                Database=db_name, UID=db_name, PWD='Qwerty12!')
    cursor = connection.cursor()
    return cursor, connection


def create_tables():
    create_cursor, create_connection = connect_to_server('arieln')
    create_cursor.execute("CREATE TABLE ProductsInventory(productID INT PRIMARY KEY,\
                        inventory INT CHECK (inventory >= 0))")

    create_cursor.execute("CREATE TABLE ProductsOrdered(transactionID VARCHAR(30),\
                    productID int,\
                    amount int CHECK (amount >= 1),\
                    FOREIGN KEY(productID) references productsInventory,\
                    PRIMARY KEY (productID,transactionID))")

    create_cursor.execute("CREATE TABLE Locks(transactionID VARCHAR(30) ,\
                    productID INT,\
                    lockType VARCHAR(5),\
                    PRIMARY KEY(transactionID, productID, lockType))")

    create_cursor.execute("CREATE TABLE Log(rowId INT NOT NULL IDENTITY PRIMARY KEY,\
                        timestamp datetime,\
                        relation varchar(30) CHECK(relation = 'ProductsInventory' or relation = 'ProductsOrdered' or relation = 'Locks'),\
                        transactionId varchar(100),\
                        productID INT,\
                        action varchar(40) CHECK(action = 'read' or action = 'update' or action = 'delete' or action = 'insert'),\
                        record varchar(2500));")
    create_cursor.commit()
    create_connection.close()


def drop_tables(cursor, con):
    cursor.execute("drop table Log")
    cursor.execute("drop table Locks")
    cursor.execute("drop table ProductsOrdered")
    con.commit()
    cursor.execute("drop table ProductsInventory")
    con.commit()


def update_inventory(transactionID):
    update_cursor, update_connection = connect_to_server('arieln')
    our_products = [i + 1 for i in range(10)]
    Z = 50 + 8
    Y = 5 + 5
    not_one_amount = int(int(Z * 10) / Y)
    one_amount = int((Z * 10) - ((Y - 1) * not_one_amount))
    # they are both 58 anyway... but we wanted to show the calculation for your benefit
    update_amounts = {}
    for i in our_products:
        if i == 0:
            update_amounts[i] = one_amount
        else:
            update_amounts[i] = not_one_amount

    our_site = Site.Site(our_products, update_amounts, update_cursor, update_connection, transactionID, 100)

    # acquire locks
    lock_status = our_site.get_all_locks()
    if lock_status == "FAILED ATTEMPT":
        print("Our site could not attain a lock for one of the products after numerous accounts.")
        print("Please try again at a later time")
        our_site.abort()
        return "FAILED ATTEMPT"

    # acquired all locks, update inventory
    need_to_add_foreign_key = our_site.update_inventory()
    # free locks used to update inventory
    our_site.remove_locks()

    if need_to_add_foreign_key:
        update_cursor.execute("ALTER TABLE Log ADD FOREIGN KEY(productID) references ProductsInventory(productID)")
        update_cursor.execute("ALTER TABLE Locks ADD FOREIGN KEY(productID) references ProductsInventory")
        update_cursor.commit()
    update_connection.close()

def manage_transactions(T):
    orders_dir = os.getcwd() + "\\orders"
    ord_cat_pro_amu_dir = {}
    for file in os.listdir(orders_dir):
        with open(orders_dir + "\\" + file) as csvfile:
            cat_pro_amu_dir = {}
            filereader = csv.reader(csvfile)
            # skip header
            next(filereader, None)
            # extract category, product and amount of order
            for row in filereader:
                if row[0] == '':
                    continue
                category = int(row[0])
                product = int(row[1])
                amount = int(row[2])

                if category in cat_pro_amu_dir.keys():
                    cat_pro_amu_dir[category][product] = amount
                else:
                    cat_pro_amu_dir[category] = {}
                    cat_pro_amu_dir[category][product] = amount
        ord_cat_pro_amu_dir[file[:-4] + "_" + str(X)] = cat_pro_amu_dir
    orders_dict = OrderedDict(sorted(ord_cat_pro_amu_dir.items()))

    order_success = {}
    for order in orders_dict:
        order_ob = Order.Order(orders_dict[order], order, T)
        try:
            order_success[order] = order_ob.activate()
        except:
            order_success[order] = "Site Error"
    # give another chance to order in case the lock was held by a different order from a different server
    for order in orders_dict:
        if order_success[order] == "TIME FAILURE":
            order_ob = Order.Order(orders_dict[order], order, T)
            try:
                order_success[order] = order_ob.activate()
            except:
                order_success[order] = "Site Error"
    print(order_success)


if __name__ == '__main__':
    private_cursor, private_connection = connect_to_server('arieln')
    drop_tables(private_cursor, private_connection)
    private_connection.commit()
    create_tables()


    update_inventory(str(7))
    manage_transactions(19)
    # private_connection.commit()
    private_connection.close()

# def update_inventory(transactionID):
#     update_cursor, update_cursor = connect_to_server('arieln')
#     our_site = Site.Site([i + 1 for i in range(10)], {}, update_cursor, transactionID, 100)
#     last_lock = 0
#
#     for p in range(10):
#         pId = p + 1
#         if sum(products) == 10:
#             update_cursor.execute(
#                 "INSERT INTO Log(timestamp ,relation, transactionId,productID, action, record) \
#                     VALUES (?,'Locks',?,?,'insert',?)",
#                 strftime("%Y-%m-%d %H:%M:%S", gmtime()), transactionID, pId, "INSERT INTO Locks VALUES (transactionId,pId,'write')")
#             update_cursor.execute(
#                 "INSERT INTO Locks VALUES (?,?,'write')",pId, transactionID)
#             update_cursor.commit()
#             update_cursor.execute("INSERT INTO Log (timestamp ,relation, transactionId,productID, action, record) \
#                                         VALUES (?,'ProductsInventory',?,? , 'update', ?)",
#                                    (strftime('%Y-%m-%d %H:%M:%S', gmtime()), str(transactionID), str(pId),
#                                     "UPDATE ProductsInventory SET inventory = 20 WHERE productID = pId"))
#             update_cursor.execute("UPDATE ProductsInventory SET inventory = 20 WHERE productID = ?",pId)
#             last_lock += 1
#             products[p] = -1
#         else:
#             gotLock = True if our_site.request_lock(pId, 'write') == 0 else False
#             i = 0
#             while not gotLock and i < max_requests:
#                 gotLock = True if our_site.request_lock(pId, 'write') == 0 else False
#                 i += 1
#             if i == max_requests:
#                 break
#             update_cursor.execute("INSERT INTO Log (timestamp ,relation, transactionId,productID, action, record) \
#                             VALUES (?,'ProductsInventory',?,? , 'insert', ?)",
#                                    (strftime('%Y-%m-%d %H:%M:%S', gmtime()), str(transactionID), str(pId),
#                                     "INSERT INTO ProductsInventory VALUES (" + str(pId) + ",20)"))
#             update_cursor.execute("INSERT INTO ProductsInventory VALUES (?,20)", pId)
#             last_lock += 1
#             products[p] = -1
#     if sum(products) == -10:
#         update_cursor.execute("SELECT * FROM ProductsInventory")
#         print(update_cursor.fetchall())
#         update_cursor.execute("SELECT * FROM Log")
#         print(update_cursor.fetchall())
#         update_cursor.execute("ALTER TABLE Log ADD FOREIGN KEY(productID) references ProductsInventory(productID)")
#         update_cursor.execute("ALTER TABLE Locks ADD FOREIGN KEY(productID) references ProductsInventory")
#         products[0] = 1
#     update_cursor.commit()
#     for p in range(last_lock):
#         if last_lock == 10:
#             products[p] = 1
#         else:
#             products[p] = 0
#         pId = p + 1
#         update_cursor.execute("INSERT INTO Log (timestamp ,relation, transactionId,productID, action, record) \
#                                     VALUES (?,'Locks',?,? , 'delete', ?)",
#                                (strftime('%Y-%m-%d %H:%M:%S', gmtime()), str(transactionID), str(pId),
#                                 "INSERT INTO ProductsInventory VALUES (" + str(pId) + ",20)"))
#         update_cursor.execute(
#             "DELETE FROM Locks \
#              WHERE productID= ?", pId)
#     update_cursor.commit()

