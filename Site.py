import queue
import threading as t
import time
from collections import deque
from time import strftime, gmtime
import pyodbc
from datetime import datetime
import time


class Site:
    # def __init__(self, site_name, prod_list, prod_dict, cursor, connection, transaction_id, timeout=200):
    def __init__(self, prod_list, prod_dict, cursor, connection, transaction_id, timeout=200):
        print(prod_list)
        self.prod_list = prod_list
        self.ready_to_change_list = []
        self.changed_list = []
        self.cursor = cursor
        self.transaction_id = str(transaction_id)
        self.timeout = timeout

        self.not_ready_queue = deque(prod_list)
        self.inventory_before_change = {}
        self.order_amount = prod_dict
        self.start_time = 0
        self.products_with_lock = []
        self.connection = connection

    # -----------------------GENERAL----------------------
    def connect_to_server(self, db_name):
        connection = pyodbc.connect(Driver='{SQL Server};', Server='technionddscourse.database.windows.net',
                                    Database=db_name, UID=db_name, PWD='Qwerty12!')
        cursor = connection.cursor()
        return cursor, connection

    def set_start_time(self, start_time):
        self.start_time = start_time

    def request_locks(self, product_id):
        """
        Check if any locks on this product. Because we want to get both read and write lock there
        must be none
        :param product_id:
        :return:
        """
        lock_product_list = self.cursor.execute("SELECT L.productID FROM Locks L WHERE L.productID = ?", product_id).fetchone()
        self.cursor.execute("INSERT INTO Log VALUES(GETDATE(), 'Locks', ?, ?, 'read', ?)",
                            self.transaction_id, product_id,
                            "SELECT L.productID FROM Locks L WHERE L.productID = " + str(product_id))
        self.cursor.commit()
        if lock_product_list is None:
            self.cursor.execute("INSERT INTO Locks values(?, ?, ?)", self.transaction_id, product_id, 'read')
            self.cursor.execute("INSERT INTO Locks values(?, ?, ?)", self.transaction_id, product_id, 'write')
            self.cursor.execute("INSERT INTO Log VALUES(GETDATE(),'Locks', ?, ?, 'insert', ?)",
                                self.transaction_id, product_id,
                                "INSERT INTO Locks values(" + self.transaction_id + ", " + str(product_id) + ", " + "read" + ")")
            self.cursor.execute("INSERT INTO Log VALUES(GETDATE(),'Locks', ?, ?, 'insert', ?)",
                                 self.transaction_id, product_id,
                                "INSERT INTO Locks values(" + self.transaction_id + "," + str(product_id) + "," + 'write' + ")")
            self.cursor.commit()
            return "TRUE"
        return "FALSE"

    def remove_locks(self, end_connection=False):
        # remove locks --DO NOT NEED TO CHECK TIME. CHECKED IN FORUM--
        lock_params = []
        log_write_params = []
        log_read_params = []
        if len(self.products_with_lock) != 0:
            for current_product in self.products_with_lock:
                lock_params.append((self.transaction_id, current_product, 'write'))
                lock_params.append((self.transaction_id, current_product, 'read'))
                log_write_params.append((self.transaction_id, current_product,
                "DELETE FROM Locks values(" + self.transaction_id + ", " + str(current_product) + ", write)"))
                log_read_params.append((self.transaction_id, current_product,
                            "DELETE FROM Locks values(" + self.transaction_id + ", " + str(current_product) + ", read)"))
            self.cursor.fast_executemany = True
            self.cursor.executemany("DELETE FROM Locks WHERE transactionID = ? AND productID = ? AND lockType = ?",
                                tuple(lock_params))
            self.cursor.fast_executemany = False
            # change fast_executemany to false before inserting to logs because of buffering problem on the record
            self.cursor.executemany("INSERT INTO Log VALUES(GETDATE(),'Locks', ?, ? ,'delete', ?)", tuple(log_write_params))
            self.cursor.executemany("INSERT INTO Log VALUES(GETDATE(),'Locks', ?, ? ,'delete', ?)", tuple(log_read_params))
            self.cursor.commit()

        if end_connection:
            self.connection.close()
        return "SUCCESS"

    # ----------------------UPDATE INVENTORY-------------------------------------
    def get_all_locks(self):
        """
        For update inventory, get locks for every product in the site and do not check the inventory
        :return:
        """
        attempt_cutoff = 5 * len(self.not_ready_queue)  # limit the amount of attempts for the site
        current_attempt = 0
        while len(self.not_ready_queue) > 0:
            current_product = self.not_ready_queue.popleft()
            got_lock = self.request_locks(current_product)
            # if failed to get lock add the product to the end of the queue, thus waiting
            if got_lock == -1:
                self.not_ready_queue.append(current_product)
            # if we did get lock then check the inventory
            else:
                # save for later that we need to free the lock on this product
                self.products_with_lock.append(current_product)
                self.ready_to_change_list.append(current_product)
            current_attempt += 1
            if current_attempt > attempt_cutoff:
                return "FAILED ATTEMPT"
        return "SUCCESS"

    def update_inventory(self):
        """
        Iterate over each item and use check_add_item to restore the default value
        :return:
        """
        need_to_add_foreign_key = False
        for current_product in self.ready_to_change_list:
            if self.check_add_item(current_product):
                need_to_add_foreign_key = True
        return need_to_add_foreign_key

    def check_add_item(self, product_id):
        """
        For a given product, checks if the product already exist in the table.
        If so, update it.
        If not, insert it.
        :param product_id:
        :return: If an item is being inserted for the first time, then we must need to update the foreign
        key that depends on ProductsInventory. Pass a boolean indicating necessary action.
        """
        need_to_add_foreign_key = False
        # check if the product id is already in ProductsInventory
        self.cursor.execute("INSERT INTO Log VALUES(GETDATE(),'ProductsInventory', ?, ? ,'read', ?)",
                            self.transaction_id, product_id,
                            "SELECT * FROM ProductsInventory P WHERE P.productID = " + str(product_id))
        empty = self.cursor.execute("SELECT * FROM ProductsInventory P WHERE P.productID = ?", product_id).fetchone()

        if empty is None:
            need_to_add_foreign_key = True
            self.cursor.execute("INSERT INTO Log VALUES(GETDATE(),'ProductsInventory', ?, ? ,'insert', ?)",
                                self.transaction_id, product_id,
                                "INSERT INTO ProductsInventory VALUES (" + str(product_id) + ", "
                                + str(self.order_amount[product_id]) + ")")
            self.cursor.execute("INSERT INTO ProductsInventory VALUES (?, ?)",
                                product_id, self.order_amount[product_id])

            self.cursor.commit()
        else:
            self.cursor.execute("UPDATE ProductsInventory SET inventory = ? WHERE productID = ?",
                                self.order_amount[product_id], product_id)
            self.cursor.execute("INSERT INTO Log VALUES(GETDATE(),'ProductsInventory', ?, ? ,'update', ?)",
                                self.transaction_id, product_id,
                                "UPDATE ProductsInventory SET inventory = " + str(self.order_amount[product_id])
                                + " WHERE productID = " + str(product_id))
            self.cursor.commit()
        return need_to_add_foreign_key

    # --------------------MANAGE TRANSACTIONS -------------------------------------
    def get_locks_and_check_inventory(self):
        """
        Perform the first stage of the carrying out a transaction on a website - making sure
        that all the products requested from that site exist in the proper quantity
        :return:
        """
        attempt_cutoff = 2 * len(self.not_ready_queue) # limit the amount of attempts for the site
        current_attempt = 0
        while len(self.not_ready_queue) > 0:
            if time.time() - self.start_time > self.timeout:
                return "TIME FAILURE"
            current_product = self.not_ready_queue.popleft()
            got_lock = self.request_locks(current_product)
            # if failed to get lock add the product to the end of the queue, thus waiting
            if got_lock == "FALSE":
                self.not_ready_queue.append(current_product)
            # if we did get lock then check the inventory
            else:
                # save for later that we need to free the lock on this product
                self.products_with_lock.append(current_product)
                # check inventory
                inventory_status = self.check_inventory(current_product)
                if inventory_status == "TIME FAILURE":
                    return "TIME FAILURE"
                elif inventory_status == "INVENTORY FAILURE":
                    return "INVENTORY FAILURE"
                else:
                    self.ready_to_change_list.append(current_product)
            current_attempt += 1
            if current_attempt > attempt_cutoff:
                return "TRY AGAIN"
        return "SUCCESS"

    def check_inventory(self, product_id):
        """
        Used by the function get_locks_and_check_inventory to check if the inventory has
        enough of the product.
        :param product_id:
        :return:
        """
        # read the inventory and check if there is enough inventory
        if time.time() - self.start_time > self.timeout:
            # fail transaction
            return "TIME FAILURE"
        self.cursor.execute("SELECT * FROM ProductsInventory P WHERE P.productID = ?", product_id)
        self.inventory_before_change[product_id] = self.cursor.fetchall()[0][1]
        self.cursor.execute("INSERT INTO Log VALUES(GETDATE(),'ProductsInventory', ?, ?, 'read', ?)",
                             self.transaction_id, product_id,
                            "SELECT p.inventory FROM ProductsInventory p WHERE p.productID = " + str(product_id))
        self.cursor.commit()
        if self.inventory_before_change[product_id] < self.order_amount[product_id]:
            return "INVENTORY FAILURE"
        return "SUCCESS"

    def change_inventory(self):
        """
        Perform stage two of an order - having checked that there is enough inventory, take that amount from
        the inventory.
        This is done using execute many, effectively making the reduction of all amount atomic.
        :return:
        """
        # create parameters to be passed to database
        product_params = []
        log_params = []
        orders_params = []
        for current_product in self.ready_to_change_list:
            if time.time() - self.start_time > self.timeout:
                # fail transaction
                return "TIME FAILURE"
            # print(f"before change: {self.inventory_before_change[current_product]}")
            # print(f"order amount: {self.order_amount[current_product]}")
            # print(f"current product: {current_product}")
            input_amount = str(self.inventory_before_change[current_product] - self.order_amount[current_product])
            product = str(current_product)
            product_params.append((self.inventory_before_change[current_product]
                                        - self.order_amount[current_product], current_product))
            log_params.append((self.transaction_id, current_product,
                        "UPDATE ProductsInventory SET P.inventory = " +
                         input_amount + " WHERE p.productID = " + product))
            orders_params.append((self.transaction_id, current_product, self.order_amount[current_product]))
            log_params.append((self.transaction_id, current_product,
                                    "INSERT INTO ProductsOrdered values(" + self.transaction_id + ", " +
                                    product + ", " + input_amount + ")"))
        if time.time() - self.start_time > self.timeout:
            # fail transaction
            return "TIME FAILURE"
        self.cursor.fast_executemany = True
        self.cursor.executemany("UPDATE ProductsInventory SET inventory = ? WHERE productID = ?",
                                tuple(product_params))
        self.cursor.executemany("INSERT INTO ProductsOrdered values(?, ?, ?)", tuple(orders_params))
        self.cursor.commit()
        self.cursor.fast_executemany = False
        self.cursor.executemany("INSERT INTO Log VALUES(GETDATE(),'ProductsInventory', ?, ? ,'update', ?)",
                                tuple(log_params))
        self.changed_list = list(self.ready_to_change_list)

        return "SUCCESS"

    def abort(self):
        """
        Should only be used in manage transactions!
        Checks if there are products that were changed in the inventory (using self.changed_list)
        if so returns them to their original state.
        Whether or not there are products that were changed, it uses the remove locks function to
        remove all the locks taken and closes connection to site
        :return:
        """
        # restore changed values to original state
        product_params = []
        log_params = []
        if len(self.changed_list) > 0:
            for current_product in self.changed_list:
                product_params.append((self.inventory_before_change[current_product], current_product))
                log_params.append((self.transaction_id, current_product,
                                    "UPDATE TABLE ProductsInventory SET inventory = " +
                                    str(self.inventory_before_change[current_product]) +
                                    " WHERE productID = " + str(current_product)
                                    ))
            self.cursor.fast_executemany = True
            self.cursor.executemany("UPDATE ProductsInventory SET inventory = ? WHERE productID = ?",
                                tuple(product_params))
            self.cursor.executemany("INSERT INTO Log VALUES(GETDATE(),'ProductsInventory', ?, ? ,'update', ?)", tuple(log_params))
            self.cursor.commit()
            self.cursor.fast_executemany = False

        # remove locks all locks taken
        self.remove_locks()

        # close connection to site
        self.connection.close()
