import time
import Site
import multiprocessing as mp
from multiprocessing.pool import ThreadPool
import queue
import Order
import pyodbc
import threading as th
from multiprocessing import Manager

from collections import deque


def connect_to_server(db_name):
    connection = pyodbc.connect(Driver='{SQL Server};', Server='technionddscourse.database.windows.net',
                                Database=db_name, UID=db_name, PWD='Qwerty12!')
    cursor = connection.cursor()
    return cursor, connection


class Order:
    def __init__(self, cat_pro_amu_dir, transaction_id, timeout):
        main_cursor, main_connection = connect_to_server('dbteam')
        self.sites = []
        for cat in cat_pro_amu_dir.keys():
            main_cursor.execute("SELECT c.siteName \
                                 FROM CategoriesToSites c \
                                 WHERE c.categoryID = " + str(cat))
            site_name = main_cursor.fetchall()[0][0]
            site_cursor, site_connection = connect_to_server(site_name)
            self.sites.append(Site.Site(cat_pro_amu_dir[cat].keys(), cat_pro_amu_dir[cat], site_cursor,
                              site_connection, transaction_id, timeout))
        main_connection.close()

        self.timeout = timeout
        self.check_site_queue = deque(self.sites)
        self.ready_for_change_queue = deque()
        self.changed_list = []
        self.start_time = 0

    def activate(self):
        self.start_time = time.time()
        # start clock for sites
        for site in self.sites:
            site.set_start_time(self.start_time)
        # FOR EACH SITE: acquire locks and check if the inventory can perform the order
        while len(self.check_site_queue) > 0:
            # perhaps alter this amount because if there is no time to finish order than we should stop early
            if time.time() - self.start_time > self.timeout:
                self.abort()
                return "TIME FAILURE"
            current_site = self.check_site_queue.popleft()
            site_status = current_site.get_locks_and_check_inventory()
            if site_status == "TIME FAILURE":
                self.abort()
                return "TIME FAILURE"
            elif site_status == "INVENTORY FAILURE":
                self.abort()
                return "INVENTORY FAILURE"
            else:
                self.ready_for_change_queue.append(current_site)
        # after checking that each site has enough inventory, change the inventory of each site
        while len(self.ready_for_change_queue) > 0:
            if time.time() - self.start_time > self.timeout:
                self.abort()
                return "TIME FAILURE"
            current_site = self.ready_for_change_queue.popleft()
            site_status = current_site.change_inventory()
            if site_status == "TIME FAILURE":
                self.abort()
                return "TIME FAILURE"
            else:
                self.changed_list.append(current_site)
        # at this point, even if there is a timeout there is no need to stop process - SEE FORUM
        for current_site in self.changed_list:
            current_site.remove_locks(True)
        return "SUCCESS"

    def abort(self):
        for site in self.sites:
            site.abort()
